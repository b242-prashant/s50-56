import React from 'react';

//create a context object

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;

