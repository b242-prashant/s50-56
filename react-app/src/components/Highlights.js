import{ Row, Col, Card } from 'react-bootstrap';
export default function Hightlights() {
	return (
		<Row>

		{/*1st card*/}
			<Col xs={12} md={4}>
				   <Card className="cardHighlight p-3">
			      	<Card.Body>
			        <Card.Title>
			        	<h2> Learn from Home </h2>
			        </Card.Title>
			        <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur 
			          ipiscing elit. Cras tempus, purus vel
			           blandit dapibus, felis risus congue nulla,
			            sit amet aliquam massa nunc ac magna. 
			            Donec imperdiet mauris ut gravida sagittis. Quisque consequat fringilla lectus. Ut ac nisi commodo ipsum dictum lobortis sit amet in turpis.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			<Col xs={12} md={4}>
				   <Card className="cardHighlight p-3">
			      	<Card.Body>
			        <Card.Title>
			        	<h2> Study now, Pay later! </h2>
			        </Card.Title>
			        <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur 
			          ipiscing elit. Cras tempus, purus vel
			           blandit dapibus, felis risus congue nulla,
			            sit amet aliquam massa nunc ac magna. 
			            Donec imperdiet mauris ut gravida sagittis. Quisque consequat fringilla lectus. Ut ac nisi commodo ipsum dictum lobortis sit amet in turpis.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			<Col xs={12} md={4}>
				   <Card className="cardHighlight p-3">
			      	<Card.Body>
			        <Card.Title>
			        	<h2> Be part of our Community </h2>
			        </Card.Title>
			        <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur 
			          ipiscing elit. Cras tempus, purus vel
			           blandit dapibus, felis risus congue nulla,
			            sit amet aliquam massa nunc ac magna. 
			            Donec imperdiet mauris ut gravida sagittis. Quisque consequat fringilla lectus. Ut ac nisi commodo ipsum dictum lobortis sit amet in turpis.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}